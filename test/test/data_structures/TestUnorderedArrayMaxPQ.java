package test.data_structures;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.*;

import model.data_structures.UnorderedArrayMaxPQ;

public class TestUnorderedArrayMaxPQ<Key> {

	private final static int TAM = 100000;
	private UnorderedArrayMaxPQ pq = new UnorderedArrayMaxPQ<>(TAM*2);
	private Comparable<?> c;
	@Before
	public void setUp1() {
		Random r = new Random();
		for(int i = 0; i < TAM; i++) {
			c = r.nextDouble()*TAM;
			pq.insert(c);
		}
	}
	@Test
	public void testDelMax() {
		int before = pq.size();
		pq.delMax();
		int after = pq.size();
		assertEquals(after, before-1);
	}
	
	@Test
	public void testInsert() {
		Random r = new Random();
		c = r.nextDouble()*TAM;
		int before = pq.size();
		pq.insert(c);
		int after = pq.size();
		assertEquals(after, before+1);
	}
	@Test
	public void testIsEmpty() {
		assertFalse(pq.isEmpty());
	}
	@Test
	public void testMax() {
		Random r = new Random();
		c = r.nextDouble()*TAM;
		pq.insert(c);
		assertEquals(c,pq.max());
	}
	
	@Test
	public void testSize() {
		assertEquals(TAM, pq.size());
	}
}
