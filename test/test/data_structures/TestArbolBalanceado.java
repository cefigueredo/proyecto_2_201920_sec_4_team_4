package test.data_structures;
import static org.junit.Assert.*;

import java.util.Random;

import org.junit.*;

import model.data_structures.ArbolBalanceado;

public class TestArbolBalanceado {
	private static final int TAM = 100000;
	private ArbolBalanceado ab = new ArbolBalanceado<>();
	private Object key;
	private Object val;
	@SuppressWarnings("unchecked")
	@Before
	public void setUp1() {
		Random r = new Random();
		for(int i = 0; i < TAM; i ++) {
			key = (int)(r.nextDouble()*TAM);
			val = (int)(r.nextDouble()*TAM*10);
			ab.put((Comparable) key, val);
		}
	}
	@Test
	public void testGet() {
		assertEquals(val,ab.get((Comparable) key));
	}
	@Test
	public void testDelete() {
		ab.delete((Comparable) key);
		assertEquals(null, ab.get((Comparable) key));
	}
	@Test
	public void testPut() {
		Random r = new Random();
		key = (int)(r.nextDouble()*TAM);
		val = (int)(r.nextDouble()*TAM*10);
		ab.put((Comparable) key, val);
		assertEquals(val,ab.get((Comparable) key));
	}
}
