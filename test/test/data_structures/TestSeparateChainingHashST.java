package test.data_structures;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.*;

import model.data_structures.SeparateChainingHashST;

public class TestSeparateChainingHashST {
	private final static int TAM = 100000;
	private Object key;
	private Object val;
	private SeparateChainingHashST<Object, Object> st = new SeparateChainingHashST<>();
	
	@Before
	public void setUp1() {
		Random r = new Random();
		
		for(int i = 0; i < TAM; i++) {
			key = (int)(r.nextDouble()*TAM);
			val = (int)(r.nextDouble()*TAM*10);
			st.put(key, val);
		}
	}

	@Test
	public void testGet() {
		assertTrue(st.get(key)!=null);
	}
	@Test
	public void testPut() {
		Random r = new Random();
		key = (int)(r.nextDouble()*TAM);
		val = (int)(r.nextDouble()*TAM*10);
		st.put(key, val);
		assertEquals(val, st.get(key));
	}
	@Test
	public void testDelete() {
		st.delete(key);
		assertEquals(null, st.get(key));
	}
}
