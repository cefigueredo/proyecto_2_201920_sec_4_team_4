package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.junit.experimental.theories.FromDataPoints;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;

import com.google.gson.*;

import com.opencsv.CSVReader;


import model.data_structures.*;
import model.data_structures.SeparateChainingHashST.Node;
/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo<T> {
	
	private final static int N = 20;
	/**
	 * Atributos del modelo del mundo
	 */
	private ArregloDinamico<UBERTripMonth> dataMonth;
	private ArregloDinamico<UBERTripDay> dataDow;
	private ArregloDinamico<UBERTripHour> dataHod;
	private ArregloDinamico<NodeMallaVial> dataEsquinas;
	private ArregloDinamico<Zone> dataZonas;
	private String[] coordenadas;
	private int trimestreAnio;
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		dataMonth = new ArregloDinamico<UBERTripMonth>(1);
		dataDow = new ArregloDinamico<UBERTripDay>(1);
		dataHod = new ArregloDinamico<UBERTripHour>(1);
		dataEsquinas=new ArregloDinamico<NodeMallaVial>(1);
		dataZonas=new ArregloDinamico<Zone>(1);
		

	}
	
	public MVCModelo(int capacidad) {
		// TODO Auto-generated constructor stub
		dataMonth = new ArregloDinamico<UBERTripMonth>(capacidad);
		dataDow = new ArregloDinamico<UBERTripDay>(capacidad);
		dataHod = new ArregloDinamico<UBERTripHour>(capacidad);
		dataEsquinas=new ArregloDinamico<NodeMallaVial>(capacidad);
		dataZonas=new ArregloDinamico<Zone>(capacidad);
		
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public String cargarDatos() throws Exception {
		System.out.println("Cargando datos...");
		long begin = System.currentTimeMillis();
		int numeroTrimestre = 1;
		String[] lineMonthly = null;
		String[] lineWeekly = null;
		String[] lineHourly = null;
	
		CSVReader readerMonth = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+numeroTrimestre+"-All-MonthlyAggregate.csv"));
		CSVReader readerWeek = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+numeroTrimestre+"-WeeklyAggregate.csv"));
		CSVReader readerDay = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+numeroTrimestre+"-All-HourlyAggregate.csv"));
	
		readerMonth.readNext();
		readerWeek.readNext();
		readerDay.readNext();
	
		int mTrip = 0;
		while((lineMonthly = readerMonth.readNext()) != null) {
			UBERTripMonth dato = new UBERTripMonth(Double.parseDouble(lineMonthly[0]), Double.parseDouble(lineMonthly[1]), Double.parseDouble(lineMonthly[2]), Double.parseDouble(lineMonthly[3]), Double.parseDouble(lineMonthly[4]), Double.parseDouble(lineMonthly[5]), Double.parseDouble(lineMonthly[6]));
			dataMonth.agregar(dato);
			mTrip++;
		}
		int dTrip = 0;
		while((lineWeekly = readerWeek.readNext()) != null) {
			
			UBERTripDay dato = new UBERTripDay(Double.parseDouble(lineWeekly[0]), Double.parseDouble(lineWeekly[1]), Double.parseDouble(lineWeekly[2]), Double.parseDouble(lineWeekly[3]), Double.parseDouble(lineWeekly[4]), Double.parseDouble(lineWeekly[5]), Double.parseDouble(lineWeekly[6]));
			dataDow.agregar(dato);
			dTrip++; 
		}
		int hTrip = 0;
		while((lineHourly = readerDay.readNext()) != null) {
			
			UBERTripHour dato = new UBERTripHour(Double.parseDouble(lineHourly[0]), Double.parseDouble(lineHourly[1]), Double.parseDouble(lineHourly[2]), Double.parseDouble(lineHourly[3]), Double.parseDouble(lineHourly[4]), Double.parseDouble(lineHourly[5]), Double.parseDouble(lineHourly[6]));
			dataHod.agregar(dato);
			hTrip++;
		}
		double minZone = 99999;
		double maxZone = 0;
		for( int i = 0; i < dataMonth.darTamano(); i++) {
			if(dataMonth.darElemento(i).getDstid() < minZone) {
				minZone = dataMonth.darElemento(i).getDstid();
			}
			if(dataMonth.darElemento(i).getSourceid() < minZone) {
				minZone = dataMonth.darElemento(i).getSourceid();
			}
			if(dataMonth.darElemento(i).getDstid() > maxZone) {
				maxZone = dataMonth.darElemento(i).getDstid();
			}
			if(dataMonth.darElemento(i).getSourceid() > maxZone) {
				maxZone = dataMonth.darElemento(i).getSourceid();
			}
		}
		
		for( int i = 0; i < dataDow.darTamano(); i++) {
			if(dataDow.darElemento(i).getDstid() < minZone) {
				minZone = dataDow.darElemento(i).getDstid();
			}
			if(dataDow.darElemento(i).getSourceid() < minZone) {
				minZone = dataDow.darElemento(i).getSourceid();
			}
			if(dataDow.darElemento(i).getDstid() > maxZone) {
				maxZone = dataDow.darElemento(i).getDstid();
			}
			if(dataDow.darElemento(i).getSourceid() > maxZone) {
				maxZone = dataDow.darElemento(i).getSourceid();
			}
		}
		
		for( int i = 0; i < dataHod.darTamano(); i++) {
			if(dataHod.darElemento(i).getDstid() < minZone) {
				minZone = dataHod.darElemento(i).getDstid();
			}
			if(dataHod.darElemento(i).getSourceid() < minZone) {
				minZone = dataHod.darElemento(i).getSourceid();
			}
			if(dataHod.darElemento(i).getDstid() > maxZone) {
				maxZone = dataHod.darElemento(i).getDstid();
			}
			if(dataHod.darElemento(i).getSourceid() > maxZone) {
				maxZone = dataHod.darElemento(i).getSourceid();
			}
		}
		numeroTrimestre = 2;
		readerMonth = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+numeroTrimestre+"-All-MonthlyAggregate.csv"));
		readerWeek = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+numeroTrimestre+"-WeeklyAggregate.csv"));
		readerDay = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+numeroTrimestre+"-All-HourlyAggregate.csv"));
	
		readerMonth.readNext();
		readerWeek.readNext();
		readerDay.readNext();
	

		while((lineMonthly = readerMonth.readNext()) != null) {
			UBERTripMonth dato = new UBERTripMonth(Double.parseDouble(lineMonthly[0]), Double.parseDouble(lineMonthly[1]), Double.parseDouble(lineMonthly[2]), Double.parseDouble(lineMonthly[3]), Double.parseDouble(lineMonthly[4]), Double.parseDouble(lineMonthly[5]), Double.parseDouble(lineMonthly[6]));
			dataMonth.agregar(dato);
			mTrip++;
		}

		while((lineWeekly = readerWeek.readNext()) != null) {
			
			UBERTripDay dato = new UBERTripDay(Double.parseDouble(lineWeekly[0]), Double.parseDouble(lineWeekly[1]), Double.parseDouble(lineWeekly[2]), Double.parseDouble(lineWeekly[3]), Double.parseDouble(lineWeekly[4]), Double.parseDouble(lineWeekly[5]), Double.parseDouble(lineWeekly[6]));
			dataDow.agregar(dato);
			dTrip++; 
		}

		while((lineHourly = readerDay.readNext()) != null ) {
			
			UBERTripHour dato = new UBERTripHour(Double.parseDouble(lineHourly[0]), Double.parseDouble(lineHourly[1]), Double.parseDouble(lineHourly[2]), Double.parseDouble(lineHourly[3]), Double.parseDouble(lineHourly[4]), Double.parseDouble(lineHourly[5]), Double.parseDouble(lineHourly[6]));
			dataHod.agregar(dato);
			hTrip++; 
		}
	
		for( int i = 0; i < dataMonth.darTamano(); i++) {
			if(dataMonth.darElemento(i).getDstid() < minZone) {
				minZone = dataMonth.darElemento(i).getDstid();
			}
			if(dataMonth.darElemento(i).getSourceid() < minZone) {
				minZone = dataMonth.darElemento(i).getSourceid();
			}
			if(dataMonth.darElemento(i).getDstid() > maxZone) {
				maxZone = dataMonth.darElemento(i).getDstid();
			}
			if(dataMonth.darElemento(i).getSourceid() > maxZone) {
				maxZone = dataMonth.darElemento(i).getSourceid();
			}
		}
		
		for( int i = 0; i < dataDow.darTamano(); i++) {
			if(dataDow.darElemento(i).getDstid() < minZone) {
				minZone = dataDow.darElemento(i).getDstid();
			}
			if(dataDow.darElemento(i).getSourceid() < minZone) {
				minZone = dataDow.darElemento(i).getSourceid();
			}
			if(dataDow.darElemento(i).getDstid() > maxZone) {
				maxZone = dataDow.darElemento(i).getDstid();
			}
			if(dataDow.darElemento(i).getSourceid() > maxZone) {
				maxZone = dataDow.darElemento(i).getSourceid();
			}
		}
		
		for( int i = 0; i < dataHod.darTamano(); i++) {
			if(dataHod.darElemento(i).getDstid() < minZone) {
				minZone = dataHod.darElemento(i).getDstid();
			}
			if(dataHod.darElemento(i).getSourceid() < minZone) {
				minZone = dataHod.darElemento(i).getSourceid();
			}
			if(dataHod.darElemento(i).getDstid() > maxZone) {
				maxZone = dataHod.darElemento(i).getDstid();
			}
			if(dataHod.darElemento(i).getSourceid() > maxZone) {
				maxZone = dataHod.darElemento(i).getSourceid();
			}
		}
		
		
		String reporte = "Total viajes en el archivo de meses: "+mTrip+"\nTotal viajes en el archivo de d�as: "+dTrip+"\nTotal viajes en el archivo de horas: "+hTrip+"\nZona con menor id identificado: "+minZone+"\nZona con mayor id identificado: "+maxZone;
		BufferedReader nodeReader= new BufferedReader( new FileReader(new File("./data/Nodes_of_red_vial-wgs84_shp.txt")));
		String line;
		int esquinas=0; 
		while((line=nodeReader.readLine())!=null )
		{
			String[] aux=line.split(",");
			NodeMallaVial actual=new NodeMallaVial(Integer.parseInt(aux[0]), Double.parseDouble(aux[1]), Double.parseDouble(aux[2]));
			esquinas++;
			dataEsquinas.agregar(actual); 
		}
		reporte+="\nSe cargaron "+esquinas+" esquinas.";
		@SuppressWarnings("deprecation")
		ArregloDinamico<T> arreglo= new ArregloDinamico<T>(1); 
		NodeMallaVial newN = null;
		Zone zona=new Zone(null, 0, "", 0, 0, null);
		JsonParser parser = new JsonParser();
		JsonObject obj = (JsonObject)parser.parse(new FileReader("./data/bogota_cadastral.json"));
		   String ptype = obj.get("type").getAsString();
		   int cont=0;
		   JsonArray arr = (JsonArray) obj.get("features").getAsJsonArray();
		   coordenadas = new String[arr.size()];
		   for(int i =0; arr!=null && i < arr.size();i++)
		   {
		    JsonObject aux = (JsonObject) arr.get(i);
		    zona=new Zone(null, 0, "", 0, 0,null);
		   
		    JsonObject pgeometry = (JsonObject)aux.get("geometry");
		    JsonArray pcoordinates= (JsonArray)pgeometry.get("coordinates");
		    NodeMallaVial north =  null;
		    coordenadas[i] = String.valueOf(pcoordinates);
		    for(int j=0;j<pcoordinates.size();j++)
		    {
		    	
		    	arreglo.agregarEnBackground((T) ((String.valueOf(pcoordinates.get(j))).replace("[","")).replace("]", ""));
		    	cont=0;
		    	
		    	for(int h=0;h< ((String) arreglo.darElemento(j)).split(",").length;h+=2)
		    	{
		    		
		    		newN=new NodeMallaVial(0,Double.parseDouble(((String) arreglo.darElemento(j)).split(",")[h]), Double.parseDouble(((String) arreglo.darElemento(j)).split(",")[h+1]));
		    		if(h==0)north=newN;
		    		else if(north.getLatitud()<newN.getLatitud()) north=newN;
		    		zona.addNode(newN);
		    	}
		    }
		    JsonObject prop= (JsonObject) aux.get("properties");
		    JsonPrimitive mov=(JsonPrimitive)prop.get("MOVEMENT_ID");
		    String smov=mov.getAsString();
		    zona.setMovementId(Integer.parseInt(smov));
		    JsonPrimitive shapeL=(JsonPrimitive)prop.get("shape_leng");
		    String sshapeL=shapeL.getAsString();
		    zona.setShape_leng(Double.parseDouble(sshapeL));
		    JsonPrimitive shapeA=(JsonPrimitive)prop.get("shape_area");
		    String sshapeA=shapeA.getAsString();
		    zona.setShape_area(Double.parseDouble(sshapeA));
		    JsonPrimitive nom=(JsonPrimitive)prop.get("scanombre");
		    String snom=nom.getAsString();
		    zona.setScanombre(snom);
		    arreglo=new ArregloDinamico<T>(1);
		    zona.setNorth(newN);
		    dataZonas.agregarEnBackground(zona); 
		   }
		   reporte+="\nSe cargaron "+dataZonas.darTamano()+" zonas.";
		   long end = System.currentTimeMillis() - begin;
		   System.out.println("Tiempo cargando: "+(end/1000)+" segundos.");
		return reporte;
	}
	//Parte 1B
	public void buscarNmasNorte(int pN)
	{
		BinaryHeapPQ<Double, Zone> bhpq=new BinaryHeapPQ<Double, Zone>(dataZonas.darTamano()+1);
		for(int i=0;i<dataZonas.darTamano();i++)
		{
			bhpq.insert(dataZonas.darElemento(i).getNorth().getLatitud(), dataZonas.darElemento(i));
		}
		for(int i=0;i<pN && i<20;i++)
		{
			System.out.println(".");
			Zone aux=bhpq.delMax();
			int h=i+1;
			System.out.println("\nZona "+h+" "+aux.getScanombre()+". Latitud: "+aux.getNorth().getLatitud()+" Longitud: "+aux.getNorth().getLongitud());
			System.out.println("************");
		}
	}
	//Parte 2B
	public void buscarGeoLoc(double latitud, double longitud) throws Exception
	{
		String rta = "";
		int nodos = 0;
	
		SeparateChainingHashST<NodeMallaVial, NodeMallaVial> hashEsquinas=new SeparateChainingHashST<NodeMallaVial, NodeMallaVial>();
		
		for(int i = 0; i < dataEsquinas.darTamano(); i++) {

				hashEsquinas.putNMV(dataEsquinas.darElemento(i), dataEsquinas.darElemento(i),latitud,longitud);
				
			
		}
		Node[] st2= hashEsquinas.getSt();
		Node act=(Node) st2[0];
		while(act!=null && nodos<20)
		{
			nodos++;
			rta+="\n"+((NodeMallaVial) act.key).getId()+" Latitud: "+((NodeMallaVial) act.key).getLatitud()+" Longitud: "+((NodeMallaVial) act.key).getLongitud();
			act=act.next;
		}
		rta +="\nEl n�mero de nodos delimitados es: "+ nodos+"\n";
		System.out.println(rta);
		
	}
	//Parte 3B
	public void buscarDesviaci�nEst�ndar(double init,double end) throws IOException
	{
		int numeroTrimestre = 1;
		String[] lineMonthly = null;
		String[] lineWeekly = null;
		String[] lineHourly = null;
	
		CSVReader readerMonth = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+numeroTrimestre+"-All-MonthlyAggregate.csv"));
		
	
		readerMonth.readNext();
		ArbolBalanceado<Double, UBERTripMonth> newdataMonth = new ArbolBalanceado<Double, UBERTripMonth>();
	
		int mTrip = 0;
		while((lineMonthly = readerMonth.readNext()) != null ) {
			UBERTripMonth dato = new UBERTripMonth(Double.parseDouble(lineMonthly[0]), Double.parseDouble(lineMonthly[1]), Double.parseDouble(lineMonthly[2]), Double.parseDouble(lineMonthly[3]), Double.parseDouble(lineMonthly[4]), Double.parseDouble(lineMonthly[5]), Double.parseDouble(lineMonthly[6]));
			
			newdataMonth.put(dato.getStandardDeviationTravelTime(), dato);;
			mTrip++;
		}
		System.out.println(newdataMonth.inordenAcotadoMonth(init,end));
		
		
		
		
	}
	
	//Parte 1A
	public String obtenerNLetrasFrecuentesNombreZona( int n ) {
		String rta = "Las " + n + " letras que m�s aparecen son: ";
		BinaryHeapPQ<Double, Zone> bhpq=new BinaryHeapPQ<Double, Zone>(dataZonas.darTamano()+1);
		for(int i=0;i<dataZonas.darTamano();i++)
		{
			bhpq.insert((double)dataZonas.darElemento(i).getMovementId(), dataZonas.darElemento(i));
		}
		
		
		int[] abc = new int[26];
		for(int i = 0; i < abc.length; i++) abc[i] = 0;
		

		for(int i = 0; i < bhpq.getSize(); i++) {
			char[] c = bhpq.getValue(i+1).getScanombre().toCharArray();
			char cprima = c[0];
			if(cprima > 96) {
				cprima = (char) (cprima - 32);
			}
			abc[cprima-65]++;
		}
		
		int mayor = 0;
		int[] letras = new int[n];
		int posMayor = 0;
		for(int j = 0; j < n; j++) {
			for(int i = 0; i < abc.length; i++) {
				if(abc[i]>mayor) {
					mayor = abc[i];
					posMayor = i;
					letras[j] = i;
				}
			}
			mayor = 0;
			rta+= " "+((char)(letras[j]+65))+",";
			abc[posMayor] = 0;
		}
		
		return rta;
	}
	//Parte 2A
	public String bucarNodosDelimitanZona(double longitud, double latitud) {
		String rta = "El n�mero de nodos delimitados es: ";
		double[] longitudes = new double[dataZonas.darTamano()];
		double[] latitudes = new double[dataZonas.darTamano()];
		String[] nombres = new String[dataZonas.darTamano()];
		int nodos = 0;
		
		SeparateChainingHashST<Object, Object> zonas = new SeparateChainingHashST<Object, Object>();
		for(int i = 0; i < dataZonas.darTamano(); i++) {
			zonas.put(dataZonas.darElemento(i).getMovementId(), dataZonas.darElemento(i));
		}	
		longitud = ((int)(longitud*1000));
		longitud = longitud/1000.0; 
		latitud = ((int)(latitud*1000));
		latitud = latitud/1000.0;
		double lon = 0;
		double lat = 0;
		for(int i = 0; i < dataZonas.darTamano(); i++) {
			coordenadas[i] = coordenadas[i].replace("[", "");
			coordenadas[i] = coordenadas[i].replace("]", "");
			System.out.println(coordenadas[i]);
			String[] sp = coordenadas[i].split(",");
			System.out.println(sp.length+" "+sp[0]+ " "+sp[1]);
			for(int j = 0; j < sp.length && sp[j+1]!=null; j+=2) {
				lon = ((int)(Double.parseDouble(sp[j])*1000));
				lon = lon/1000.0;
				lat = ((int)(Double.parseDouble(sp[j+1])*1000));
				lat = lat/1000.0;
				if(lon == longitud && lat == latitud) {
					nodos++;
					nombres[i] = dataZonas.darElemento(i).getScanombre();
					longitudes[i] = lon;
					latitudes[i] = lat;
					j = sp.length;
				}
			}		
		}
		
		rta += nodos+".\n";
		for(int i = 0; i < nombres.length && nombres[i] != null; i++) {
			rta+="\nPara "+nombres[i]+": Longitud: "+longitudes[i]+" Latitud: "+latitudes[i];
		}
		
		return rta;
	}
	//Parte 3A
	public String buscarTiemposPromedioEnRangoEnPrimerSemestre(int limBajo, int limAlto) {
		String rta = "Los "+N+" viajes en rango en el primer semestre del 2018 son: ";
		int encontrados = 0;
		for(int i = 0;i < dataMonth.darTamano() && dataMonth.darElemento(i)!=null && encontrados < N; i++) {
			if(dataMonth.darElemento(i).getMeanTravelTime() > limBajo && dataMonth.darElemento(i).getMeanTravelTime() < limAlto && dataMonth.darElemento(i).getMonth() < 4) {
				encontrados++;
				rta+="\n"+encontrados+".  Zona or�gen: "+dataMonth.darElemento(i).getSourceid()+
						"\nZona destino: "+dataMonth.darElemento(i).getDstid()+
						"\nMes: "+dataMonth.darElemento(i).getMonth()+
						"\nTiempo promedio mensual: "+dataMonth.darElemento(i).getMeanTravelTime()+"\n";
			}
		}
		return rta;
	}
	
	
	//Parte 1C
	public String retornarTiempoPromedioPorZonaYHora(double zona, int hora) {
		String rta = "";
		System.out.println(dataHod.darElemento(1).getSourceid()+" "+dataHod.darElemento(1).getHod());
		for(int i = 0; i < dataHod.darTamano(); i++) {
			if(zona == dataHod.darElemento(i).getSourceid() && hora == dataHod.darElemento(i).getHod()) {
				rta +="Zona origen: "+dataHod.darElemento(i).getSourceid()+" zona destino: "+dataHod.darElemento(i).getDstid()+" hora: "+dataHod.darElemento(i).getHod()+" tiempo promedio: "+dataHod.darElemento(i).getMeanTravelTime() +"\n";
			}
		}
		
		return rta;
	}
	//Parte 2C
	public void zonaRangoHoras(int dstid,int init,int end)
	{
		ArbolBalanceado<Integer, UBERTripHour> arb=new ArbolBalanceado<Integer, UBERTripHour>();
		int cont=0;
		String rta="";
		for(int i=0;i<dataHod.darTamano() ;i++)
		{
			if(dataHod.darElemento(i).getDstid()==dstid)
			{
			arb.put((int) dataHod.darElemento(i).getHod(), dataHod.darElemento(i));
			}
		}
		System.out.println(arb.inordenAcotadoDay(init, end));
	}
	//Parte 3C
	public void zonasPorCantidadNodos(int N)
	{
		BinaryHeapPQ<Integer, Zone> bhpq= new BinaryHeapPQ<Integer, Zone>(dataZonas.darTamano()+1);
		int cont=0;
		String rta="";
		Zone a=null;
		for(int i=0;i<dataZonas.darTamano();i++)
		{
			bhpq.insert(dataZonas.darElemento(i).getNodos().darTamano(), dataZonas.darElemento(i));
		}
		for(int i=0;i<dataZonas.darTamano() && cont<20;i++)
		{
			a=bhpq.delMax();
			rta+="\nNombre: "+a.getScanombre()+" N�mero de nodos: "+a.getNodos().darTamano();
			cont++;
		}
		System.out.println(rta);
	}
	//Parte 4C
	public String graficarPorcentajeFaltante() {
		String rta = "";
		int esperado = dataHod.darTamano()*(dataHod.darTamano()-1)*24*2;
		int[] zonas = new int[dataHod.darTamano()];
		for(int i = 0; i < zonas.length; i++) zonas[i] = 0;
		System.out.println("Esperado: "+esperado);
		for(int i = 0; i < dataHod.darTamano(); i++) {	
			System.out.println((int)dataHod.darElemento(i).getSourceid());
			int pos = (int) dataHod.darElemento(i).getSourceid();
			zonas[pos] = zonas[pos]+1;
		}
		
		for(int i = 0; i < zonas.length; i++) {
			
			rta+= "\n"+i+"| ";
			for(int j = 0; j < (esperado -zonas[i])*50/esperado; j++) {
				rta += "*";
			}
			
		}
		return rta;
	}
}
