package model.logic;

public class NodeMallaVial implements Comparable<NodeMallaVial> {
	private int id;
	private double longitud;
	private double latitud;
	public NodeMallaVial(int pId, double pLongitud,double pLatitud) {
		// TODO Auto-generated constructor stub
		id=pId;
		longitud=pLongitud;
		latitud=pLatitud;
	} 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getLongitud() {
		return longitud;
	}
	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}
	public double getLatitud() {
		return latitud;
	}
	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}
	@Override
	public int compareTo(NodeMallaVial Node) {
		// TODO Auto-generated method stub
		if(Node.getId()<id)
		{
			return 1;
		}
		else if(Node.getId()>id)
		{
			return 1;
		}
		else
			return 0;
	}
	
	
	
}
