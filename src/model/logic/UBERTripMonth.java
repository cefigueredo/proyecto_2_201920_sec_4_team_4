package model.logic;

public class UBERTripMonth extends UBERTrip {

	private double month;
	public UBERTripMonth(double pSourceid, double pDstid, double pMonth, double pMeanTravelTime, double pStandardDeviationTravelTime,
			double pGeometricMeanTravelTime, double pGeometricStandardDeviationTravelTime) {
		super(pSourceid, pDstid, pMeanTravelTime, pStandardDeviationTravelTime, pGeometricMeanTravelTime,
				pGeometricStandardDeviationTravelTime);
		month = pMonth;
	}
	public double getMonth() {
		return month;
	}

}
