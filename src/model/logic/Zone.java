package model.logic;

import model.data_structures.ArregloDinamico;

public class Zone implements Comparable<Zone>{
	private int MovementId;
	private String scanombre;
	private double shape_leng;
	private double shape_area;
	private NodeMallaVial north;
	private ArregloDinamico<NodeMallaVial> nodos;
	public Zone(ArregloDinamico<NodeMallaVial> pNodes,int pid,String pNombre, double pPerimetro, double pArea, NodeMallaVial pNorth)
	{
		if(pNodes!=null)
		setNodos(pNodes);
		else
			setNodos(new ArregloDinamico<NodeMallaVial>(1));
		MovementId=pid;
		scanombre=pNombre;
		shape_leng=pPerimetro;
		shape_area=pArea;
		north=pNorth;
	}
	public NodeMallaVial getNorth() {
		return north;
	}
	public void setNorth(NodeMallaVial north) {
		this.north = north;
	}
	public int getMovementId() {
		return MovementId;
	}
	public void setMovementId(int movementId) {
		MovementId = movementId;
	}
	public String getScanombre() {
		return scanombre;
	}
	public void setScanombre(String scanombre) {
		this.scanombre = scanombre;
	}
	public double getShape_leng() {
		return shape_leng;
	}
	public void setShape_leng(double shape_leng) {
		this.shape_leng = shape_leng;
	}
	public double getShape_area() {
		return shape_area;
	}
	public void setShape_area(double shape_area) {
		this.shape_area = shape_area;
	}
	public void addNode(NodeMallaVial newN)
	{
		getNodos().agregar(newN);
	}
	@Override
	public int compareTo(Zone o) {
		// TODO Auto-generated method stub
		if(north.getLatitud()>o.getNorth().getLatitud())return 1;
		else if(north.getLatitud()<o.getNorth().getLatitud())return -1;
		else return 0;
			
	}
	public int compareToB(Zone z)
	{
		if(getNodos().darTamano()>z.getNodos().darTamano()) return 1;
		else if(getNodos().darTamano()<z.getNodos().darTamano()) return -1;
		else return 0;
	}
	public ArregloDinamico<NodeMallaVial> getNodos() {
		return nodos;
	}
	public void setNodos(ArregloDinamico<NodeMallaVial> nodos) {
		this.nodos = nodos;
	}
}
