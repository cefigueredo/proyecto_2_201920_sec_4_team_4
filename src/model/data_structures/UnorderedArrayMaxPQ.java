package model.data_structures;

import model.logic.Zone;

public class UnorderedArrayMaxPQ <K extends Comparable<K>> {
	private K[] pq; // pq[i] = ith element on pq
	private int N; // number of elements on pq
	@SuppressWarnings("unchecked")
	public UnorderedArrayMaxPQ (int capacity) {
		pq = (K[]) new Comparable [ capacity ]; }
	
	public boolean isEmpty() { return N == 0; }
	
	@SuppressWarnings("unchecked")
	public UnorderedArrayMaxPQ(ArregloDinamico<Zone> data)
	{
		pq = (K[]) new Zone[data.darTamano()+1 ];
		N=0;
		for(int i=0; i<1160;i++)
		{
			insert((K) data.darElemento(i));
		}
	}
	
	

	public void insert(K x) { pq[N++] = x;
	}
	
	public K max() {
		int max = 0;
		for (int i = 1; i < N; i++)
			if (less(max, i)) max = i;
		return pq[max];
	}
	
	public K delMax(){
		int max = 0;
		for (int i = 1; i < N; i++)
			if (less(max, i)) max = i;
		exch(max, N-1);
		return pq[--N];
	}
	public K delMaxB(){
		int max = 0;
		for (int i = 1; i < N; i++)
			if (lessB(max, i)) max = i;
		exch(max, N-1);
		return pq[--N];
	}

	public int size() {
		return N;
	}
	
	public boolean less(int a, int b) {
		if(pq[a].compareTo(pq[b])<0)
		{
			return true;
		}
		else return false;
	}
	public boolean lessB(int a, int b) {
		if(((Zone) pq[a]).compareToB((Zone) pq[b])<0)
		{
			return true;
		}
		else return false;
	}
	
	public void exch(int pos1, int pos2) {
		while(pq!=null)
		{
		K aux =pq[pos1];
		pq[pos1]=pq[pos2];
		pq[pos2]=aux;
		}
	}
}
