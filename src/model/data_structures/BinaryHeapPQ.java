package model.data_structures;

import model.logic.Zone;

public class BinaryHeapPQ <Key extends Comparable<Key>, Value> {
	private Key[] pq;
	private Value[] va;
	private int N;
	public BinaryHeapPQ(int capacidad)
	{
		pq = (Key[]) new Comparable [ capacidad ];
		va= (Value[]) new Zone [capacidad];
	}
	private boolean less (int i, int j)
	{
	return pq[i].compareTo(pq[j]) < 0;
	}
	private void exch (int i, int j)
	{
	Key t = pq[i];
	Value v=va[i];
	pq[i] = pq[j];
	va[i]=va[j];
	pq[j] = t;
	va[j]=v;
	
	}
	public void insert (Key x, Value zone)
	{
		pq[++N] = x;
		va[N]=zone;
		swim(N);
	}
	
	private void swim (int k)
	{
		while (k > 1 && less(k/2, k))
		{
			exch(k, k/2);
			k = k/2;
		}
	}
	
	public Value delMax ()
	{
		Value max = va[1];
		exch(1, N--);
		sink(1);
		pq[N+1] = null;
		va[N+1]=null;
		return max;
	}
	
	private void sink (int k)
	{
		while (2*k <= N)
		{
			int j = 2*k;
			if (j < N && less(j, j+1))
				j++;
			if (!less(k, j))
				break;
			exch(k, j);
			k = j;
		}
	}
	public Value getValue(int n) {
		return va[n];
	}
	
	public Key getKey(int n) {
		return pq[n];
	}
	
	public int getSize() {
		return N;
	}
}
