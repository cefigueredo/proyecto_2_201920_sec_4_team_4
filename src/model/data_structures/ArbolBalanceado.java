package model.data_structures;

import java.util.ArrayList;

import model.logic.UBERTrip;
import model.logic.UBERTripDay;
import model.logic.UBERTripHour;
import model.logic.UBERTripMonth;

public class ArbolBalanceado<Key extends Comparable<Key>, Value> {

	private static final boolean RED = true;
	private static final boolean BLACK = false;
	private Node root;
	private ArrayList<Value> inorden=new ArrayList<Value>();
	private class Node{
		Key key; // key
		Value val; // associated data
		Node left, right; // subtrees
		int N; // # nodes in this subtree
		int size;
		boolean color; // color of link from
		// parent to this node

		Node(Key key2, Value val2, boolean red, int i) {
			this.key = key2;
			this.val = val2;
			this.N = i;
			this.color = red;
		}

	}

	public int size()
	{ return size(root); }
	private int size(Node x)
	{
		if (x == null) return 0;
		return x.size;
	}

	private boolean isRed(Node x)
	{
		if (x == null)
			return false;
		return x.color == RED;
	}


	private Node rotateLeft(Node h)
	{
		Node x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = h.color;
		h.color = RED;
		x.N = h.N;
		h.N = 1 + size(h.left) + size(h.right);
		return x;
	}

	private Node rotateRight(Node h)
	{
		Node x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = h.color;
		h.color = RED;
		x.N = h.N;
		h.N = 1 + size(h.left) + size(h.right);
		return x;
	}

	private void flipColors(Node h)
	{
		h.color = RED;
		h.left.color = BLACK;
		h.right.color = BLACK;
	}
	public void put(Key key, Value val) {
		if (key == null) throw new
		IllegalArgumentException("first argument to put() is null");
		if (val == null) {
			delete(key);
			return;
		}
		root = put(root, key, val);
		root.color = BLACK;
	}

	public void delete(Key key) { root = delete(root, key); }
	private Node delete(Node x, Key key) {
		if (x == null) return null;
		int cmp = key.compareTo(x.key);
		if (cmp < 0) x.left = delete(x.left, key);
		else if (cmp > 0) x.right = delete(x.right, key);
		else {
			if (x.right == null) return x.left;
			if (x.left == null) return x.right;
			Node t = x;
			x = min(t.right);
			x.right = deleteMin(t.right);
			x.left = t.left;
		}
		x.size= size(x.left) + size(x.right) + 1;
		return x;
	}
	public void min() {
		root = min(root);
	}
	private Node min(Node x) {
		if(x.left == null) return x;
		x.left = min(x.left);
		return x;
	}
	public void deleteMin()
	{ root = deleteMin(root); }
	private Node deleteMin(Node x)
	{
		if (x.left == null) return x.right;
		x.left = deleteMin(x.left);
		x.size= 1 + size(x.left) + size(x.right);
		return x;
	}
	
	@SuppressWarnings("unchecked")
	private Node put(Node h, Key key, Value val) {
		if (h == null)
			return new Node(key, val, RED, 1);
		int cmp = ( key).compareTo(h.key);
		if (cmp < 0)
			h.left = put(h.left, key, val);
		else if (cmp > 0)
			h.right = put(h.right, key, val);
		else
			h.val = val;
		// fix-up any right-leaning links
		if (isRed(h.right) && !isRed(h.left)) h = rotateLeft(h);
		if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
		if (isRed(h.left) && isRed(h.right)) flipColors(h);
		h.size = size(h.left) + size(h.right) + 1;
		return h;
	}
	
	@SuppressWarnings("unchecked")
	public Value get(Key key)
	{
		Node x = root;
		while (x != null)
		{
			int cmp = ( key).compareTo(x.key);
			if (cmp < 0) x = x.left;
			else if (cmp > 0) x = x.right;
			else if (cmp == 0) return x.val;
		}
		return null;
	}
	public String inordenAcotadoMonth(double init, double end) {
		// TODO Auto-generated method stub
		String rta="";
		inorden (root);
		int con=0;
		for(int i=0;i<inorden.size() && con<20;i++)
		{
			if(((UBERTripMonth) inorden.get(i)).getStandardDeviationTravelTime()>=init && ((UBERTrip) inorden.get(i)).getStandardDeviationTravelTime()<=end)
			{
				con++;
				rta+="\nSourceId: "+((UBERTrip) inorden.get(i)).getSourceid()+" DstId: "+((UBERTrip) inorden.get(i)).getDstid()
						+" Month: "+((UBERTripMonth) inorden.get(i)).getMonth()+" Desviación estándar: "+((UBERTrip) inorden.get(i)).getStandardDeviationTravelTime();
			}
		}
		return rta;
	}
	public String inordenAcotadoDay(double init, double end) {
		// TODO Auto-generated method stub
		String rta="";
		inorden (root);
		int con=0;
		for(int i=0;i<inorden.size() && con<20;i++)
		{
			
			if(((UBERTripHour) inorden.get(i)).getHod()>=init && ((UBERTripHour) inorden.get(i)).getHod()<=end)
			{
				
				con++;
				rta+="\nSourceId: "+((UBERTrip) inorden.get(i)).getSourceid()+" DstId: "+((UBERTrip) inorden.get(i)).getDstid()
						+" Hour: "+((UBERTripHour) inorden.get(i)).getHod()+" Tiempo de viaje: "+((UBERTrip) inorden.get(i)).getMeanTravelTime();
			}
		}
		return rta;
	}
	private void inorden(Node nod)
	{
		Node x=nod;
		if(x.left!=null)
		{
			inorden(x.left);
		}
		inorden.add((Value) x.val);
		if(x.right!=null)
		{
			inorden(x.right);
		}
	}


}
