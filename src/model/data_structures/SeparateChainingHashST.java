package model.data_structures;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import model.logic.NodeMallaVial;

public class SeparateChainingHashST<Key, Value> {
	private int M = 97; // number of chains
	private Node[] st = new Node[M]; // array of chains
	/************************************************************************************************/
	public static class Node<Key, Value> {

		public Object key;
		private Object val;
		public Node next;

		public Node(Key key2, Value val2, Node node) {
			key = key2;
			val = val2;
			next = node;
		}
	}
	/************************************************************************************************/
	private int hash(Key key) {return (key.hashCode() & 0x7fffffff) % M; }
	public int hashNMV(Key key,double lat,double longit) throws ParseException
	{
		NumberFormat nf = NumberFormat.getInstance(); 
		nf.setMaximumFractionDigits(2);
		nf.setRoundingMode(RoundingMode.DOWN);
		String st = nf.format(((NodeMallaVial) key).getLatitud());
		NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
		Number number = format.parse(st);
		double lati = number.doubleValue();
		st = nf.format(((NodeMallaVial) key).getLongitud());
		 format = NumberFormat.getInstance(Locale.FRANCE);
		 number = format.parse(st);
		double lon = number.doubleValue();
		
		if (lati == lat && lon == longit) 
		{
			return 0;
		}
		else if(((key.hashCode() & 0x7fffffff)%M)==0) return 1;
		else return hash(key);
		
	}
	public Value get(Key key) {
		int i = hash(key);
		for (Node x = getSt()[i]; x != null; x = x.next)
			if (key.equals(x.key)) return (Value) x.val;
		return null;
	}
	
	public void put(Key key, Value val)
	{
		int i = hash(key);
		for (Node x = getSt()[i]; x != null; x = x.next)
			if (key.equals(x.key))
			{
				x.val = val;
				return;
			}
		getSt()[i] = new Node(key, val, getSt()[i]);
	}
	public void putNMV(Key key, Value val, double lat,double lon) throws Exception
	{

		int i = hashNMV(key,lat,lon);
		for (Node x = getSt()[i]; x != null; x = x.next)
			if (key.equals(x.key))
			{
				x.val = val;
				return;
			}
		getSt()[i] = new Node(key, val, getSt()[i]);
	}
	
	public int size() {
		return getSt().length;
	}
	
	public Key delete(Key key) {
		put(key, null);
		return key;
	}
	public Node[] getSt() {
		return st;
	}
	public void setSt(Node[] st) {
		this.st = st;
	}
}