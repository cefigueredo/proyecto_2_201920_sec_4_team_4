package controller;

import java.util.Locale;
import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() throws Exception 
	{
		Scanner lector = new Scanner(System.in);
		
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					
				    
				    modelo = new MVCModelo(); 
					System.out.println(modelo.cargarDatos());
										
					break;

				case 2:
					System.out.println("Ingrese la longitud (m�ximo 2 d�gitos): ");
					double longituda = lector.nextDouble();
					System.out.println("Ingrese la latitud (m�ximo 2 d�gitos): ");
					double latituda = lector.nextDouble();
					modelo.buscarGeoLoc(latituda, longituda);
					System.out.println("Finished");
											
					break;

				case 3:
					System.out.println("Ingrese el l�mite inferior: ");
					double liminf = lector.nextDouble();
					System.out.println("Ingrese el l�mite superior: ");
					double limsup = lector.nextDouble();	
					modelo.buscarDesviaci�nEst�ndar(liminf, limsup);
					break;
				case 4:
					System.out.print("\nIngrese el n�mero de zonas a mostrar");
					int n=lector.nextInt();
					modelo.buscarNmasNorte(n);
					break;
				case 5: 
					System.out.println("\nIngrese el dstid: ");
					int dstid=lector.nextInt();
					System.out.println("\nIngrese el l�mite inferior: ");
					int liminfb=lector.nextInt();
					System.out.println("Ingrese el l�mite superior: ");
					int limsupb = lector.nextInt();
					modelo.zonaRangoHoras(dstid, liminfb, limsupb);
					break;	
					
				case 6: 
					System.out.println("Ingrese n�mero de letras frecuentes a buscar: ");
					int m = lector.nextInt();
					String rta = modelo.obtenerNLetrasFrecuentesNombreZona(m);
					System.out.println(rta);
					break;
					
				case 7: 
					System.out.println("Ingrese la longitud (con 3 decimales): ");
					double longitud = lector.nextDouble();
					System.out.println("Ingrese la latitud (con 3 decimales): ");
					double latitud = lector.nextDouble();
					String r = modelo.bucarNodosDelimitanZona(longitud, latitud);
					System.out.println(r);
					break;	
					
				case 8: 
					System.out.println("Ingrese l�mite de tiempo inferior (en segundos): ");
					int limI = lector.nextInt();
					System.out.println("Ingrese l�mite de tiempo superior (en segundos): ");
					int limS = lector.nextInt();
					String r1 = modelo.buscarTiemposPromedioEnRangoEnPrimerSemestre(limI, limS);
					System.out.println(r1);
					break;
					
				case 9: 
					System.out.print("\nIngrese el n�mero de zonas a mostrar");
					int nb=lector.nextInt();
					modelo.zonasPorCantidadNodos(nb);
					break;	
					
				case 10: 
					System.out.println("\nIngrese id de la zona: ");
					double z = lector.nextDouble();
					System.out.println("\nIngrese hora: ");
					int h = lector.nextInt();
					String rm = modelo.retornarTiempoPromedioPorZonaYHora(z, h);
					System.out.println(rm);
					break;	
					
				case 11: 
					System.out.println("Realizando gr�fica.");
					String rrr = modelo.graficarPorcentajeFaltante();
					System.out.println(rrr);
					break;	
				case 12: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
