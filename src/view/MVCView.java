package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Cargar datos");
			System.out.println("2. Buscar por localizaci�n geogr�fica - 2B");
			System.out.println("3. Buscar en rango por desviaci�n est�ndar - 3B");
			System.out.println("4. Buscar las zonas m�s al norte - 1B");
			System.out.println("5. Viajes hacia una zona dada en rango de hora - 2C");
			System.out.println("6. Obtener N letras m�s frecuentes en zona - 1A");
			System.out.println("7. Buscar nodos que delimitan zonas - 2A");
			System.out.println("8. Buscas tiempos promedio en rango en prime trimestre del 2018 - 3A");
			System.out.println("9. Buscar zonas por cantidad de nodos - 3C");
			System.out.println("10. Retornar tiempos promedio por zona y hora dadas - 1C");
			System.out.println("11. Gr�fica ASCII porcentaje de datos faltantes - 4C");
			System.out.println("12. Exit");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			// TODO implementar
		}
}
